using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentApiController : ControllerBase
    {
        List<Venda> listaVendas = new List<Venda>();

        Venda venda = new Venda();

        DateTime dataAtual = DateTime.Now;

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(Vendedor vendedor, List<string>itens)
        {

            if(itens == null)
            {
                return NotFound("O registro de uma venda deve possuir pelo menos 1 item");
            }

            venda.Vendedor = vendedor;
            venda.ItensVendidos = itens;
            venda.Data = dataAtual;
            venda.Status = "Aguardando pagamento...";

            listaVendas.Add(venda);
            
            string serializado = JsonConvert.SerializeObject(venda, Formatting.Indented);

            return Ok(venda);
        }

        [HttpGet("BuscarVenda/{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var encontre = listaVendas.Find(venda => venda.Id ==id);

            if(encontre == null)
            {
                return NotFound();
            }

            return Ok(encontre);
        }

        [HttpPut("AtualizarVenda/{id}")]
        public IActionResult AtualizarVenda(int id, string status)
        {
            var encontre = listaVendas.Find(venda => venda.Id == id);

            if(encontre == null)
            {
                return NotFound();
            }

            if(encontre.Status == "Aguardando pagamento...")
            {
                if(status == "Pagamento aprovado" || status == "Cancelada")
                {
                    venda.Status = status;
                }
            }else if(encontre.Status == "Pagamento aprovado")
            {
                if(status == "Enviado para a transportadora" || status == "Cancelada")
                {
                    venda.Status = status;
                }
            }else if(encontre.Status == "Enviando para a transportadora")
                if(status == "Entregue")
                {
                    venda.Status = status;
                }


            return Ok(encontre);

        }
    }
}